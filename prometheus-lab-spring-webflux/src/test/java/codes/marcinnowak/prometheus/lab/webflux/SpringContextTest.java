package codes.marcinnowak.prometheus.lab.webflux;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.util.UriComponentsBuilder;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(stubs = "classpath:/stubs/mappings", port = 0)
public class SpringContextTest {

    @LocalServerPort
    private int port;

    @Autowired
    private WebTestClient testClient;

    @Test
    void shouldQueryRangeIncrease() {
        // Given
        // When
        PrometheusQueryRangeResponse response = testClient.get()
                .uri(UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "lab_increase(some_counter_metric[5s])")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString())
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(PrometheusQueryRangeResponse.class)
                .returnResult()
                .getResponseBody();

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "5.0")
                                .value(1600000008, "5.0")
                                .value(1600000009, "5.0")
                                .value(1600000010, "5.0")
                                .value(1600000011, "5.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "5.0")
                                .value(1600000018, "5.0")
                                .value(1600000019, "5.0")
                                .value(1600000020, "5.0")
                                .value(1600000021, "5.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "5.0")
                                .value(1600000028, "5.0")
                                .value(1600000029, "5.0")
                                .value(1600000030, "5.0")
                                .value(1600000031, "5.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

}
