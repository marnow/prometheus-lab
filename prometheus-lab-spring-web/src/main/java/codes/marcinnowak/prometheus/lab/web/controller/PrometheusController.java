package codes.marcinnowak.prometheus.lab.web.controller;

import codes.marcinnowak.prometheus.lab.web.config.Properties;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;
import codes.marcinnowak.prometheus.lab.promql.PQLParser;
import codes.marcinnowak.prometheus.lab.promql.PromDataRequest;
import codes.marcinnowak.prometheus.lab.promql.PromEvent;
import codes.marcinnowak.prometheus.lab.web.service.PrometheusService;
import io.micrometer.core.annotation.Timed;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.Enumeration;

@Timed
@Slf4j
@RestController
@AllArgsConstructor
public class PrometheusController {
    private final PrometheusService prometheusService;
    private final Properties properties;

    @RequestMapping("/api/v1/query_range")
    public PrometheusQueryRangeResponse queryRange(@RequestParam String query,
                                                   @RequestParam BigDecimal start,
                                                   @RequestParam BigDecimal end,
                                                   @RequestParam int step) {

        PrometheusQueryRequest queryRequest = PrometheusQueryRequest.builder()
                .query(URLDecoder.decode(query))
                .start(start)
                .end(end)
                .step(step)
                .build();

        if (!(query.contains("lab_increase") || (query.contains("lab_nincrease")))) {
            return prometheusService.queryRange(step, queryRequest);
        }

        PQLParser pqlParser = new PQLParser();
        pqlParser.handleIncreaseByVisitor(queryRequest);

        while (pqlParser.hasNext()) {
            PromEvent event = pqlParser.next();
            if (event instanceof PromDataRequest) {
                PromDataRequest promDataRequest = (PromDataRequest) event;
                PrometheusQueryRangeResponse response = prometheusService.queryRange(promDataRequest.getStep(), PrometheusQueryRequest.builder()
                        .query(promDataRequest.getQuery())
                        .start(promDataRequest.getStart())
                        .end(promDataRequest.getEnd())
                        .step(promDataRequest.getStep())
                        .underline(promDataRequest.getUnderline())
                        .build());
                pqlParser.onDataRequestResponse(response);
            }
        }

        return pqlParser.getPrometheusQueryRangeResponse();
    }

    @RequestMapping("/graph")
    public ResponseEntity mirrorRest(@RequestBody(required = false) String body,
                                     HttpMethod method,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws URISyntaxException {
        String requestUrl = request.getRequestURI();

        URI uri = new URI(properties.getPrometheus().getBaseUrl());
        uri = UriComponentsBuilder.fromUri(uri)
                .path(requestUrl)
                .query(request.getQueryString())
                .build(true).toUri();

        HttpHeaders headers = new HttpHeaders();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            headers.set(headerName, request.getHeader(headerName));
        }

        HttpEntity<String> httpEntity = new HttpEntity<>(body, headers);
        RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate.exchange(uri, method, httpEntity, String.class);
        } catch (HttpStatusCodeException e) {
            return ResponseEntity.status(e.getRawStatusCode())
                    .headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
        }
    }

    @RequestMapping("/static/**")
    public ResponseEntity mirrorStatic(@RequestBody(required = false) String body,
                                       HttpMethod method,
                                       HttpServletRequest request,
                                       HttpServletResponse response) throws URISyntaxException {
        String requestUrl = request.getRequestURI();

        URI uri = new URI(properties.getPrometheus().getBaseUrl());
        uri = UriComponentsBuilder.fromUri(uri)
                .path(requestUrl)
                .query(request.getQueryString())
                .build(true).toUri();

        HttpHeaders headers = new HttpHeaders();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            headers.set(headerName, request.getHeader(headerName));
        }

        HttpEntity<String> httpEntity = new HttpEntity<>(body, headers);
        RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate.exchange(uri, method, httpEntity, String.class);
        } catch (HttpStatusCodeException e) {
            return ResponseEntity.status(e.getRawStatusCode())
                    .headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
        }
    }

    @RequestMapping("/api/v1/label/**")
    public ResponseEntity mirrorLabel(@RequestBody(required = false) String body,
                                      HttpMethod method,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws URISyntaxException {
        return proxyRequest(body, method, request, properties);
    }

    @RequestMapping("/api/v1/query")
    public ResponseEntity mirrorQuery(@RequestBody(required = false) String body,
                                      HttpMethod method,
                                      HttpServletRequest request,
                                      HttpServletResponse response) throws URISyntaxException {
        return proxyRequest(body, method, request, properties);
    }

    public static ResponseEntity<String> proxyRequest(String body, HttpMethod method, HttpServletRequest request, Properties properties) throws URISyntaxException {
        String requestUrl = request.getRequestURI();

        URI uri = new URI(properties.getPrometheus().getBaseUrl());
        uri = UriComponentsBuilder.fromUri(uri)
                .path(requestUrl)
                .query(request.getQueryString())
                .build(true).toUri();

        HttpHeaders headers = new HttpHeaders();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            if ("Accept-Encoding".equals(headerName))
                continue;
            headers.set(headerName, request.getHeader(headerName));
        }

        HttpEntity<String> httpEntity = new HttpEntity<>(body, headers);
        RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate.exchange(uri, method, httpEntity, String.class);
        } catch (HttpStatusCodeException e) {
            return ResponseEntity.status(e.getRawStatusCode())
                    .headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
        }
    }

}
