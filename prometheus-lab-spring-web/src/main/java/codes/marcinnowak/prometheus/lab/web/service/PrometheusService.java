package codes.marcinnowak.prometheus.lab.web.service;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;

public interface PrometheusService {
    PrometheusQueryRangeResponse queryRange(int step, PrometheusQueryRequest promQueryRange);
}
