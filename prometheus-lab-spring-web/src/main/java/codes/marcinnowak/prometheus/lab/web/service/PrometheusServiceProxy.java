package codes.marcinnowak.prometheus.lab.web.service;

import codes.marcinnowak.prometheus.lab.web.controller.Util;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;
import org.springframework.web.client.RestOperations;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.NotEmpty;
import java.net.URI;

public class PrometheusServiceProxy implements PrometheusService {

    private final RestOperations restTemplate;
    private  String baseUrl;

    public PrometheusServiceProxy(RestOperations restTemplate, @NotEmpty String baseUrl) {
        this.restTemplate = restTemplate;
        this.baseUrl = baseUrl;
    }

    @Override
    public PrometheusQueryRangeResponse queryRange(int step, PrometheusQueryRequest promQueryRange) {
        UriComponentsBuilder prometheusQueryRangeUri = Util.toQueryRangeUriBuilder(promQueryRange, this.baseUrl);

        final URI uri = prometheusQueryRangeUri
                .replaceQueryParam("step", step)
                .build()
                .encode()
                .toUri();
        return this.restTemplate.getForObject(uri, PrometheusQueryRangeResponse.class);
    }
}
