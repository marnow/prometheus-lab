package codes.marcinnowak.prometheus.lab.web.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Validated
@Component
@ConfigurationProperties("prometheus-lab")
public class Properties {
    private PrometheusProperties prometheus;

    private SimulatorProperties simulator;

    private RetryProperties retry;

    @Data
    public static class SimulatorProperties{
        @NotNull
        private Boolean enable;
    }

    @Data
    public static class PrometheusProperties{
        @NotEmpty
        private String baseUrl;
    }

    @Data
    public static class RetryProperties{
        @NotNull
        private Boolean enable;

        @NotNull
        private Integer maxAttempts;
    }

}
