# Prometheus-Lab

Prometheus PromQL proxy fulfilled singnal processing rules and bypass limitaton described https://github.com/prometheus/prometheus/issues/3806

Use environment variable `PROMETHEUS_URL` to set Prometheus host.

# Run Prometheus

```shell script
docker run --rm --name prometheus -p 9090:9090 -v $PWD/config:/etc/prometheus prom/prometheus
```

Prometheus in debug mode:

```shell script
docker run --rm --name prometheus -p 9090:9090 -v $PWD/config:/etc/prometheus --entrypoint /bin/prometheus prom/prometheus --log.level=debug --config.file=/etc/prometheus/prometheus.yml --storage.tsdb.path=/prometheus
```


```shell script
docker run --rm --name prom-pushgateway -it -p 9091:9091 prom/pushgateway
```

```shell script
echo -e "# TYPE some_counter_metric counter\nsome_counter_metric{label=\"val1\"} 15\n" | curl --data-binary @- http://localhost:9091/metrics/job/some_job/instance/some_instance
```

```shell script
echo -e "# TYPE some_counter_metric counter\nsome_counter_metric{label=\"val1\"} 50\nsome_counter_metric{label=\"val2\"} 18\n" | curl --data-binary @- http://localhost:9091/metrics/job/some_job/instance/some_instance
```
```shell script
echo -e "# TYPE some_counter_metric counter\nsome_counter_metric{country=\"Poland\",device=\"car1\",instance=\"some_instance\",job=\"some_job\",location=\"Warszawa\"} 65\nsome_counter_metric{country=\"Poland\",device=\"car1\",instance=\"some_instance\",job=\"some_job\",location=\"Cracow\"} 30\nsome_counter_metric{country=\"Germany\",device=\"car1\",instance=\"some_instance\",job=\"some_job\",location=\"Berlin\"} 30\nsome_counter_metric{country=\"Germany\",device=\"car1\",instance=\"some_instance\",job=\"some_job\",location=\"Hamburg\"} 30\n" | curl --data-binary @- http://localhost:9091/metrics/job/some_job/instance/some_instance
```


```shell script
docker run --name grafana -p 3000:3000 grafana/grafana
```

```shell script
docker build -t prometheus-lab .
```

```shell script
docker run --rm --name prometheus-lab -p 8080:8080 prometheus-lab
```

Docker up with build prometheus-lab:
```shell script
docker-compose up --build
```

```shell script
docker-compose down
```


Command do start docker compose for stability test:
```shell script
docker-compose --file docker-compose-disturber.yml up --build
```


# Run Pit 

```shell script
mvn org.pitest:pitest-maven:mutationCoverage
```
