package codes.marcinnowak.prometheus.lab.simulator;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class PrometheusSimulatorRandomTest {
    @Test
    void shouldQueryRangeStep1() {
        // Given
        final PrometheusSimulatorRandom prometheusSimulatorRandom = new PrometheusSimulatorRandom(5, 1_234L);
        // When
        final PrometheusQueryRangeResponse prometheusQueryRangeResponse = prometheusSimulatorRandom.queryRange(PrometheusQueryRequest.builder()
                .query("http_requests")
                .start(BigDecimal.valueOf(1))
                .end(BigDecimal.valueOf(5))
                .step(1)
                .build());
        //Then
        assertThat(prometheusQueryRangeResponse).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1L, "4")
                                .value(2L, "8")
                                .value(3L, "10")
                                .value(4L, "11")
                                .value(5L, "12")
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldQueryRangeStep2() {
        // Given
        final PrometheusSimulatorRandom prometheusSimulatorRandom = new PrometheusSimulatorRandom(5, 1_234L);
        // When
        final PrometheusQueryRangeResponse prometheusQueryRangeResponse = prometheusSimulatorRandom.queryRange(PrometheusQueryRequest.builder()
                .query("http_requests")
                .start(BigDecimal.valueOf(1))
                .end(BigDecimal.valueOf(9))
                .step(2)
                .build());
        //Then
        assertThat(prometheusQueryRangeResponse).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1L, "4")
                                .value(3L, "8")
                                .value(5L, "10")
                                .value(7L, "11")
                                .value(9L, "12")
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldQueryRangeIncrease3() {
        // Given
        final PrometheusSimulatorRandom prometheusSimulatorRandom = new PrometheusSimulatorRandom(3, 1_234L);
        // When
        final PrometheusQueryRangeResponse prometheusQueryRangeResponse = prometheusSimulatorRandom.queryRange(PrometheusQueryRequest.builder()
                .query("http_requests")
                .start(BigDecimal.valueOf(1))
                .end(BigDecimal.valueOf(5))
                .step(1)
                .build());
        //Then
        assertThat(prometheusQueryRangeResponse).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1L, "2")
                                .value(2L, "4")
                                .value(3L, "5")
                                .value(4L, "6")
                                .value(5L, "6")
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldQueryRangeDifferentSeed() {
        // Given
        final PrometheusSimulatorRandom prometheusSimulatorRandom = new PrometheusSimulatorRandom(5, 12_321L);
        // When
        final PrometheusQueryRangeResponse prometheusQueryRangeResponse = prometheusSimulatorRandom.queryRange(PrometheusQueryRequest.builder()
                .query("http_requests")
                .start(BigDecimal.valueOf(1))
                .end(BigDecimal.valueOf(5))
                .step(1)
                .build());
        //Then
        assertThat(prometheusQueryRangeResponse).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1L, "1")
                                .value(2L, "2")
                                .value(3L, "4")
                                .value(4L, "4")
                                .value(5L, "7")
                                .build())
                        .build())
                .build());
    }
}
