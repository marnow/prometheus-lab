package codes.marcinnowak.prometheus.lab.promql;

import codes.marcinnowak.prometheus.lab.calculation.PromTimeStamp;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse.ResponseData;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse.ResponseData.ResponseResult;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static codes.marcinnowak.prometheus.lab.Utility.sample;
import static org.assertj.core.api.Assertions.assertThat;

@TestMethodOrder(MethodOrderer.Random.class)
class IncreaseTest {

    @Test
    void shouldIncreaseStepEqualInterval_1() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),  // interval
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),  // interval
                sample(1600000005, "10"), // interval
                sample(1600000006, "10"), // interval
                sample(1600000007, "10"), // interval
                sample(1600000008, "10"), // interval
                sample(1600000009, "16"), // interval
                sample(1600000010, "16"), // interval
                sample(1600000011, "16")  // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(1), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),  // interval
                sample(1600000003, "0"),  // interval
                sample(1600000004, "0"),  // interval
                sample(1600000005, "5"),  // interval
                sample(1600000006, "0"),  // interval
                sample(1600000007, "0"),  // interval
                sample(1600000008, "0"),  // interval
                sample(1600000009, "6"),  // interval
                sample(1600000010, "0"),  // interval
                sample(1600000011, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldIncreaseStepEqualInterval_2() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000003, "5"),  // interval
                sample(1600000005, "5"),  // interval
                sample(1600000007, "5"),  // interval
                sample(1600000009, "10"), // interval
                sample(1600000011, "10"), // interval
                sample(1600000013, "10"), // interval
                sample(1600000015, "10"), // interval
                sample(1600000017, "16"), // interval
                sample(1600000019, "16"), // interval
                sample(1600000021, "16")  // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 2, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000021"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000003, "0"),  // interval
                sample(1600000005, "0"),  // interval
                sample(1600000007, "0"),  // interval
                sample(1600000009, "5"),  // interval
                sample(1600000011, "0"),  // interval
                sample(1600000013, "0"),  // interval
                sample(1600000015, "0"),  // interval
                sample(1600000017, "6"),  // interval
                sample(1600000019, "0"),  // interval
                sample(1600000021, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldIncreaseWithoutIntervalReminder() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "10"), // interval
                sample(1600000006, "10"),
                sample(1600000007, "10"), // interval
                sample(1600000008, "10"),
                sample(1600000009, "15"), // interval
                sample(1600000010, "15"),
                sample(1600000011, "15")  // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "5"),  // interval
                sample(1600000006, "0"),
                sample(1600000007, "0"),  // interval
                sample(1600000008, "5"),
                sample(1600000009, "5"),  // interval
                sample(1600000010, "0"),
                sample(1600000011, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldIncreaseEndIntervalReminderNotWholeWithZeroDiff() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "10"), // interval
                sample(1600000006, "10"),
                sample(1600000007, "10"), // interval
                sample(1600000008, "10"),
                sample(1600000009, "15"), // interval
                sample(1600000010, "15"),
                sample(1600000011, "15"), // interval
                sample(1600000012, "15")
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000012"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "5"),  // interval
                sample(1600000006, "0"),
                sample(1600000007, "0"),  // interval
                sample(1600000008, "5"),
                sample(1600000009, "5"),  // interval
                sample(1600000010, "0"),
                sample(1600000011, "0"),  // interval
                sample(1600000012, "0")
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldIncreaseEndIntervalReminderNotWholeWithNotZeroDiff() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "10"), // interval
                sample(1600000006, "10"),
                sample(1600000007, "10"), // interval
                sample(1600000008, "10"),
                sample(1600000009, "15"), // interval
                sample(1600000010, "15"),
                sample(1600000011, "15"), // interval
                sample(1600000012, "20")
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000012"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "5"),  // interval
                sample(1600000006, "0"),
                sample(1600000007, "0"),  // interval
                sample(1600000008, "5"),
                sample(1600000009, "5"),  // interval
                sample(1600000010, "0"),
                sample(1600000011, "0"),  // interval
                sample(1600000012, "5")
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldIncreaseIntervalOdd5() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "15"), // interval
                sample(1600000002, "15"),
                sample(1600000003, "15"),
                sample(1600000004, "15"),
                sample(1600000005, "15"),
                sample(1600000006, "15"), // interval
                sample(1600000007, "15"),
                sample(1600000008, "15"),
                sample(1600000009, "15"),
                sample(1600000010, "20"),
                sample(1600000011, "20"), // interval
                sample(1600000012, "20"),
                sample(1600000013, "20"),
                sample(1600000014, "20"),
                sample(1600000015, "20"),
                sample(1600000016, "20"), // interval
                sample(1600000017, "20"),
                sample(1600000018, "20"),
                sample(1600000019, "20"),
                sample(1600000020, "25"),
                sample(1600000021, "25"), // interval
                sample(1600000022, "25"),
                sample(1600000023, "25"),
                sample(1600000024, "25"),
                sample(1600000025, "25"),
                sample(1600000026, "25"), // interval
                sample(1600000027, "25"),
                sample(1600000028, "25"),
                sample(1600000029, "25"),
                sample(1600000030, "30"),
                sample(1600000031, "30"), // interval
                sample(1600000032, "30"),
                sample(1600000033, "30"),
                sample(1600000034, "30"),
                sample(1600000035, "30"),
                sample(1600000036, "30"), // interval
                sample(1600000037, "30"),
                sample(1600000038, "30"),
                sample(1600000039, "30"),
                sample(1600000040, "30")  // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(5), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000040"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"), // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),
                sample(1600000004, "0"),
                sample(1600000005, "0"),
                sample(1600000006, "0"), // interval
                sample(1600000007, "5"),
                sample(1600000008, "5"),
                sample(1600000009, "5"),
                sample(1600000010, "5"),
                sample(1600000011, "5"), // interval
                sample(1600000012, "0"),
                sample(1600000013, "0"),
                sample(1600000014, "0"),
                sample(1600000015, "0"),
                sample(1600000016, "0"), // interval
                sample(1600000017, "5"),
                sample(1600000018, "5"),
                sample(1600000019, "5"),
                sample(1600000020, "5"),
                sample(1600000021, "5"), // interval
                sample(1600000022, "0"),
                sample(1600000023, "0"),
                sample(1600000024, "0"),
                sample(1600000025, "0"),
                sample(1600000026, "0"), // interval
                sample(1600000027, "5"),
                sample(1600000028, "5"),
                sample(1600000029, "5"),
                sample(1600000030, "5"),
                sample(1600000031, "5"), // interval
                sample(1600000032, "0"),
                sample(1600000033, "0"),
                sample(1600000034, "0"),
                sample(1600000035, "0"),
                sample(1600000036, "0"), // interval
                sample(1600000037, "0"),
                sample(1600000038, "0"),
                sample(1600000039, "0"),
                sample(1600000040, "0")  // interval
        );

        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldHandleRestartToCounterEqualToZero() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "10"), // interval
                sample(1600000006, "0"),  // <- Restart
                sample(1600000007, "0"),  // interval
                sample(1600000008, "0"),
                sample(1600000009, "5"),  // interval
                sample(1600000010, "5"),
                sample(1600000011, "5")   // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "5"),  // interval
                sample(1600000006, "0"),
                sample(1600000007, "0"),  // interval
                sample(1600000008, "5"),
                sample(1600000009, "5"),  // interval
                sample(1600000010, "0"),
                sample(1600000011, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldHandleRestartToCounterGraterThenZero() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "10"), // interval
                sample(1600000006, "2"),  // <- Restart
                sample(1600000007, "2"),  // interval
                sample(1600000008, "2"),
                sample(1600000009, "5"),  // interval
                sample(1600000010, "5"),
                sample(1600000011, "5")   // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "5"),  // interval
                sample(1600000006, "2"),
                sample(1600000007, "2"),  // interval
                sample(1600000008, "3"),
                sample(1600000009, "3"),  // interval
                sample(1600000010, "0"),
                sample(1600000011, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldHandleMissingBeginning1sample() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
//                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "11"), // interval
                sample(1600000006, "11"),
                sample(1600000007, "11"), // interval
                sample(1600000008, "11"),
                sample(1600000009, "18"), // interval
                sample(1600000010, "18"),
                sample(1600000011, "18")  // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
//                sample(1600000001, "0"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "6"),
                sample(1600000005, "6"),  // interval
                sample(1600000006, "0"),
                sample(1600000007, "0"),  // interval
                sample(1600000008, "7"),
                sample(1600000009, "7"),  // interval
                sample(1600000010, "0"),
                sample(1600000011, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldHandleMissingDataAfterMissingHigherThenBefore() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                //                    5                  interval
                //                    6
                //                    7                  interval
                //                    8
                //                    9                  interval
                sample(1600000010, "10"),
                sample(1600000011, "10"),  // interval
                sample(1600000012, "10"),
                sample(1600000013, "10"),  // interval
                sample(1600000014, "15"),
                sample(1600000015, "15"), // interval
                sample(1600000016, "15")
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000016"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "0"),
                //                    5                  interval
                //                    6
                //                    7                  interval
                //                    8
                //                    9                  interval
                sample(1600000010, "10"),
                sample(1600000011, "10"),  // interval
                sample(1600000012, "0"),
                sample(1600000013, "0"),  // interval
                sample(1600000014, "5"),
                sample(1600000015, "5"),  // interval
                sample(1600000016, "0")
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldHandleMissingDataAfterMissingLowerThenBefore() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                //                    5                  interval
                //                    6
                //                    7                  interval
                //                    8
                //                    9                  interval
                sample(1600000010, "2"),
                sample(1600000011, "2"),  // interval
                sample(1600000012, "2"),
                sample(1600000013, "2"),  // interval
                sample(1600000014, "15"),
                sample(1600000015, "15"), // interval
                sample(1600000016, "15")
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increment = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000016"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "0"),
                //                    5                  interval
                //                    6
                //                    7                  interval
                //                    8
                //                    9                  interval
                sample(1600000010, "2"),
                sample(1600000011, "2"),  // interval
                sample(1600000012, "0"),
                sample(1600000013, "0"),  // interval
                sample(1600000014, "13"),
                sample(1600000015, "13"),  // interval
                sample(1600000016, "0")
        );
        assertThat(increment).isEqualTo(expected);
    }

    @Test
    void shouldHandleMissingDataInterval5s() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
//                           1600000001,              // interval
//                           1600000002,
//                           1600000003,
//                           1600000004,
//                           1600000005,
//                           1600000006,              // interval
//                           1600000007,
                sample(1600000008, "15"),
                sample(1600000009, "20"),
                sample(1600000010, "20"),
                sample(1600000011, "20"), // interval
                sample(1600000012, "20"),
                sample(1600000013, "20"),
                sample(1600000014, "20"),
                sample(1600000015, "20"),
                sample(1600000016, "20"),
                sample(1600000017, "20"),
                sample(1600000018, "20"),
                sample(1600000019, "20"),
                sample(1600000020, "20"),
                sample(1600000021, "20"),
                sample(1600000022, "20"),
                sample(1600000023, "20"),
                sample(1600000024, "20"),
                sample(1600000025, "20"),
                sample(1600000026, "20"),
                sample(1600000027, "20"),
                sample(1600000028, "20"),
                sample(1600000029, "20"),
                sample(1600000030, "20"),
                sample(1600000031, "20"),
                sample(1600000032, "20"),
                sample(1600000033, "20"),
                sample(1600000034, "20"),
                sample(1600000035, "20"),
                sample(1600000036, "20"),
                sample(1600000037, "20"),
                sample(1600000038, "20"),
                sample(1600000039, "20"),
                sample(1600000040, "20")
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increment = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(5), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000040"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
//                           1600000001,              // interval
//                           1600000002,
//                           1600000003,
//                           1600000004,
//                           1600000005,
//                           1600000006,              // interval
//                           1600000007,
                sample(1600000008, "20"),
                sample(1600000009, "20"),
                sample(1600000010, "20"),
                sample(1600000011, "20"),  // interval
                sample(1600000012, "0"),
                sample(1600000013, "0"),
                sample(1600000014, "0"),
                sample(1600000015, "0"),
                sample(1600000016, "0"),  // interval
                sample(1600000017, "0"),
                sample(1600000018, "0"),
                sample(1600000019, "0"),
                sample(1600000020, "0"),
                sample(1600000021, "0"),  // interval
                sample(1600000022, "0"),
                sample(1600000023, "0"),
                sample(1600000024, "0"),
                sample(1600000025, "0"),
                sample(1600000026, "0"),  // interval
                sample(1600000027, "0"),
                sample(1600000028, "0"),
                sample(1600000029, "0"),
                sample(1600000030, "0"),
                sample(1600000031, "0"),  // interval
                sample(1600000032, "0"),
                sample(1600000033, "0"),
                sample(1600000034, "0"),
                sample(1600000035, "0"),
                sample(1600000036, "0"),  // interval
                sample(1600000037, "0"),
                sample(1600000038, "0"),
                sample(1600000039, "0"),
                sample(1600000040, "0")   // interval
        );
        assertThat(increment).isEqualTo(expected);
    }

    @Test
    void extractSubgroups() {
        // Given
        ResponseResult responseResult1 = ResponseResult.builder()
                .metric("__name__", "some_counter_metric")
                .metric("instance", "some_instance")
                .metric("job", "some_job")
                .metric("label", "val1")
                .metric("status", "OK")
                .value(Arrays.asList(1600000001, "1"))  // interval
                .value(Arrays.asList(1600000002, "2"))
                .value(Arrays.asList(1600000003, "3"))
                .build();

        ResponseResult responseResult2 = ResponseResult.builder()
                .metric("__name__", "some_counter_metric")
                .metric("instance", "some_instance")
                .metric("job", "some_job")
                .metric("label", "val1")
                .metric("status", "NOK")
                .value(Arrays.asList(1600000001, "4"))  // interval
                .value(Arrays.asList(1600000002, "5"))
                .value(Arrays.asList(1600000003, "6"))
                .build();

        ResponseResult responseResult3 = ResponseResult.builder()
                .metric("__name__", "some_counter_metric")
                .metric("instance", "some_instance")
                .metric("job", "some_job")
                .metric("label", "val2")
                .metric("status", "OK")
                .value(Arrays.asList(1600000001, "7"))  // interval
                .value(Arrays.asList(1600000002, "8"))
                .value(Arrays.asList(1600000003, "9"))
                .build();

        ResponseResult responseResult4 = ResponseResult.builder()
                .metric("__name__", "some_counter_metric")
                .metric("instance", "some_instance")
                .metric("job", "some_job")
                .metric("label", "val2")
                .metric("status", "OK")
                .value(Arrays.asList(1600000001, "10"))  // interval
                .value(Arrays.asList(1600000002, "11"))
                .value(Arrays.asList(1600000003, "12"))
                .build();
        ResponseData responseData = ResponseData.builder()
                .result(responseResult1)
                .result(responseResult2)
                .result(responseResult3)
                .result(responseResult4)
                .build();

        // When
        List<ResponseData> extractSubgroups = PQLParser.extractSubgroups(responseData, Arrays.asList("label"));

        // Then
        ArrayList<ResponseData> expected = new ArrayList<>();

        expected.add(ResponseData.builder()
                .result(ResponseResult.builder()
                        .metric("label", "val1")
                        .values(responseResult1.getValues())
                        .build())
                .result(ResponseResult.builder()
                        .metric("label", "val1")
                        .values(responseResult2.getValues())
                        .build())
                .build());

        expected.add(ResponseData.builder()
                .result(ResponseResult.builder()
                        .metric("label", "val2")
                        .values(responseResult3.getValues())
                        .build())
                .result(ResponseResult.builder()
                        .metric("label", "val2")
                        .values(responseResult4.getValues())
                        .build())
                .build());
        assertThat(extractSubgroups).isEqualTo(expected);
    }
}
