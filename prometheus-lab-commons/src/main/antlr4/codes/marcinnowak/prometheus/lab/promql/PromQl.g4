grammar PromQl;

expression
   : multiplyingExpression ((PLUS | MINUS) multiplyingExpression)*
   ;

multiplyingExpression
   : powExpression ((TIMES | DIV) powExpression)*
   ;

powExpression
   : signedAtom (POW signedAtom)*
   ;

signedAtom
   : PLUS signedAtom
   | MINUS signedAtom
   | func
   | sumBy
   | atom
   | intervalAtomWithFilter
   | intervalAtom
   ;

variable
   : VARIABLE
   ;

intervalAtomWithFilter
   : atom '{' filterParameters (',' filterParameters)*  '}' LSQUER (SCIENTIFIC_NUMBER TIMES)? SCIENTIFIC_NUMBER timeUnit RSQUER
   ;


filterParameters
    :  variable filterRelation filterValue
    ;


intervalAtom
   : atom LSQUER (SCIENTIFIC_NUMBER TIMES)? SCIENTIFIC_NUMBER timeUnit RSQUER
   ;

atom
   : scientific
   | variable
   | constant
   | LPAREN expression RPAREN
   ;

filterValue
    : FILTER_VARIABLE
    ;

filterRelation
    : EQ
    | '!' EQ
    | EQ '~'
    | '!~'
    ;

scientific
   : SCIENTIFIC_NUMBER
   ;

constant
   : PI
   | EULER
   | I
   ;


func
   : funcname LPAREN expression (COMMA expression)* RPAREN
   ;

sumBy
   :
   | funcname LPAREN expression (COMMA expression)* RPAREN BY LPAREN sumByExpresion RPAREN
   ;

sumByExpresion
    : expression (COMMA expression)*
    ;

funcname
   : SUM
   | INCREASE
   | INCREASEN
   ;

relop
   : EQ
   | GT
   | LT
   ;

timeUnit
  : SECOND
  | MINUTE
  | HOUR
  ;

SECOND: 's';

MINUTE: 'm';

HOUR: 'h';

INCREASE: 'lab_increase';

INCREASEN: 'lab_nincrease';

SUM: 'sum';

BY: 'by';

LPAREN
   : '('
   ;


RPAREN
   : ')'
   ;

LSQUER: '[';

RSQUER: ']';

QUOTE: '"';

PLUS
   : '+'
   ;


MINUS
   : '-'
   ;


TIMES
   : '*'
   ;


DIV
   : '/'
   ;


GT
   : '>'
   ;


LT
   : '<'
   ;


EQ
   : '='
   ;


COMMA
   : ','
   ;


POINT
   : '.'
   ;


POW
   : '^'
   ;


PI
   : 'pi'
   ;


EULER
   : E2
   ;


I
   : 'i'
   ;


FILTER_VARIABLE
    : '"' ( ~[\\\r\n\f"] )* '"'
    ;

VARIABLE
   : VALID_ID_START VALID_ID_CHAR*
   ;


fragment VALID_ID_START
   : ('a' .. 'z') | ('A' .. 'Z') | '_'
   ;


fragment VALID_ID_CHAR
   : VALID_ID_START | ('0' .. '9')
   ;


SCIENTIFIC_NUMBER
   : NUMBER ((E1 | E2) SIGN? NUMBER)?
   ;


fragment NUMBER
   : ('0' .. '9') + ('.' ('0' .. '9') +)?
   ;


fragment E1
   : 'E'
   ;


fragment E2
   : 'e'
   ;


fragment SIGN
   : ('+' | '-')
   ;


WS
   : [ \r\n\t] + -> skip
   ;
