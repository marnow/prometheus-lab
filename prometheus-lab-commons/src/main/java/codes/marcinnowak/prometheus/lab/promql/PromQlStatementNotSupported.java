package codes.marcinnowak.prometheus.lab.promql;

public class PromQlStatementNotSupported extends RuntimeException {
    public PromQlStatementNotSupported(String message) {
        super(message);
    }
}
