package codes.marcinnowak.prometheus.lab.promql;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class PromEventNull extends PromEvent {
}
