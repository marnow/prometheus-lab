package codes.marcinnowak.prometheus.lab.calculation;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.util.StringJoiner;

public final class PromTimeStamp {
    private final BigDecimal timestamp;

    public PromTimeStamp(BigDecimal timestamp) {

        this.timestamp = timestamp;
    }

    public static PromTimeStamp of(String timestamp) {
        return new PromTimeStamp(new BigDecimal(timestamp));
    }

    public static PromTimeStamp of(Object timestamp) {
        return new PromTimeStamp((BigDecimal) timestamp);
    }

    public static PromTimeStamp of(long timestamp) {
        return new PromTimeStamp(BigDecimal.valueOf(timestamp));
    }

    public static PromTimeStamp of(BigDecimal timestamp) {
        return new PromTimeStamp(timestamp);
    }

    PromTimeStamp minusSeconds(int step) {
        return subtract(of(step));
    }

    public PromTimeStamp plusSeconds(int step) {
        return add(of(step));
    }

    public PromTimeStamp plusSeconds(BigDecimal step) {
        return add(of(step));
    }

    public boolean isEarlierOrEquals(PromTimeStamp other) {
        return compareTo(other) <= 0;
    }

    public boolean isEarlierThen(PromTimeStamp other) {
        return compareTo(other) < 0;
    }

    public boolean isLaterThen(PromTimeStamp other) {
        return this.compareTo(other) > 0;
    }

    public BigDecimal toBigDecimal() {
        return timestamp;
    }

    public PromTimeStamp subtract(PromTimeStamp beginTime) {
        return PromTimeStamp.of(timestamp.subtract(beginTime.toBigDecimal()));
    }

    public PromTimeStamp add(PromTimeStamp beginTime) {
        return PromTimeStamp.of(timestamp.add(beginTime.toBigDecimal()));
    }

    public int compareTo(PromTimeStamp val) {
        return timestamp.compareTo(val.toBigDecimal());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PromTimeStamp.class.getSimpleName() + "[", "]")
                .add("timestamp=" + timestamp)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PromTimeStamp that = (PromTimeStamp) o;

        return new EqualsBuilder()
                .append(timestamp, that.timestamp)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(timestamp)
                .toHashCode();
    }
}
