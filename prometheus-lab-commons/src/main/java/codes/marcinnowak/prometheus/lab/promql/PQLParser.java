package codes.marcinnowak.prometheus.lab.promql;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.*;

public class PQLParser {

    private Iterator<ParseTree> iterator;
    private PromQlVisitorReversePolishNotationCalculation calculationVisitor;

    public static Map<String, String> metric(Map<String, String> metric, List<String> groupsBy) {
        Map<String, String> metricResult = new HashMap<>();
        for (String label : groupsBy)
            if (metric.containsKey(label))
                metricResult.put(label, metric.get(label));
        return metricResult;
    }

    public static String calculateKey(Map<String, String> metric, List<String> groupsBy) {
        StringBuilder keyBuilder = new StringBuilder();
        for (String label : groupsBy) {
            keyBuilder.append(label).append(":").append(metric.get(label)).append(",");
        }
        return keyBuilder.toString();
    }

    public static List<PrometheusQueryRangeResponse.ResponseData> extractSubgroups(PrometheusQueryRangeResponse.ResponseData results, List<String> groupsBy) {
        HashMap<String, PrometheusQueryRangeResponse.ResponseData> result = new LinkedHashMap<>();

        for (PrometheusQueryRangeResponse.ResponseData.ResponseResult responseResult : results.getResults()) {
            String key = calculateKey(responseResult.getMetrics(), groupsBy);

            result.computeIfPresent(key, (s, responseData) -> responseData.toBuilder()
                    .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                            .metrics(metric(responseResult.getMetrics(), groupsBy))
                            .values(responseResult.getValues())
                            .build())
                    .build());
            result.computeIfAbsent(key, s -> PrometheusQueryRangeResponse.ResponseData.builder()
                    .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                            .metrics(metric(responseResult.getMetrics(), groupsBy))
                            .values(responseResult.getValues())
                            .build())
                    .build());
        }

        return new ArrayList<>(result.values());
    }

    public void handleIncreaseByVisitor(PrometheusQueryRequest prometheusQueryRequest) {
        PromQlLexer lexer = new PromQlLexer(CharStreams.fromString(prometheusQueryRequest.getQuery()));
        PromQlParser parser = new PromQlParser(new CommonTokenStream(lexer));
        ParseTree tree = parser.expression();
        PromQlVisitorReverse visitor = new PromQlVisitorReverse();
        visitor.visit(tree);

        List<ParseTree> contexts = visitor.getContexts();

        calculationVisitor = new PromQlVisitorReversePolishNotationCalculation(prometheusQueryRequest);
        iterator = contexts.iterator();
    }

    public PromEvent next() {
        ParseTree context = iterator.next();
        return context.accept(calculationVisitor);
    }

    public boolean hasNext() {
        return iterator.hasNext();
    }

    public PrometheusQueryRangeResponse getPrometheusQueryRangeResponse() {
        return calculationVisitor.getPrometheusQueryRangeResponse();
    }

    public PromEventNull onDataRequestResponse(PrometheusQueryRangeResponse response) {
        return calculationVisitor.onDataRequestResponse(response);
    }
}