package codes.marcinnowak.prometheus.lab.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PrometheusQueryRangeResponse {
    String status;
    ResponseData data;

    @Data
    @Builder(toBuilder = true)
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ResponseData {
        String resultType;
        @JsonProperty("result")
        @Singular
        List<ResponseResult> results;

        @NoArgsConstructor
        public static class ResponseResult {
            @JsonProperty("metric")
            Map<String, String> metrics;

            List<List<Object>> values;

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;

                if (o == null || getClass() != o.getClass()) return false;

                ResponseResult that = (ResponseResult) o;

                return new EqualsBuilder()
                        .append(metrics, that.metrics)
                        .append(values, that.values)
                        .isEquals();
            }

            @Override
            public int hashCode() {
                return new HashCodeBuilder(17, 37)
                        .append(metrics)
                        .append(values)
                        .toHashCode();
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this)
                        .append("metrics", metrics)
                        .append("values", values)
                        .toString();
            }

            public void setMetrics(Map<String, String> metricsMap) {
                this.metrics = new HashMap<>(metricsMap);
            }

            public ResponseResult(Map<String, String> metrics, List<List<Object>> values) {

                this.metrics = metrics;
                this.values = values;
            }

            public static ResponseResultBuilder builder(){
                return new ResponseResultBuilder();
            }

            public Map<String, String> getMetrics() {
                return metrics;
            }

            public List<List<Object>> getValues() {
                return values;
            }

            public void setValues(List<List<Object>> samples) {
                values = new ArrayList<>(samples);
            }

            public static class ResponseResultBuilder{
                Map<String, String> metrics = new HashMap<>();

                List<List<Object>> values = new ArrayList<>();

                public ResponseResultBuilder metric(String key, String value) {
                    metrics.put(key, value);
                    return this;
                }

                public ResponseResultBuilder value(List<Object> samples) {
                    values.add(samples);
                    return this;
                }

                public ResponseResultBuilder value(Object... samples) {
                    this.values.add(Arrays.asList(samples));
                    return this;
                }

                public ResponseResultBuilder metrics(Map<String, String> aMetrics) {
                    metrics.putAll(aMetrics);
                    return this;
                }

                public ResponseResult build() {
                    return new ResponseResult(metrics, values);
                }

                public ResponseResultBuilder values(List<List<Object>> samples) {
                    values.addAll(samples);
                    return this;
                }
            }
        }

    }

}
