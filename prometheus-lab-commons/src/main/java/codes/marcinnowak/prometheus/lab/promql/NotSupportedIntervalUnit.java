package codes.marcinnowak.prometheus.lab.promql;

public class NotSupportedIntervalUnit extends RuntimeException {
    public NotSupportedIntervalUnit(String message) {
        super(message);
    }
}
