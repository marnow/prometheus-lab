package codes.marcinnowak.prometheus.lab.promql;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Builder
@ToString
@EqualsAndHashCode
public final class PromDataRequest extends PromEvent {
    private final String query;

    private final BigDecimal start;

    private final BigDecimal end;

    private final int step;

    private final long underline;
}

