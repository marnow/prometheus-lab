image: openjdk:13-alpine

include:
  - template: Code-Quality.gitlab-ci.yml

variables:
  # When using dind service we need to instruct docker, to talk with the
  # daemon started inside of the service. The daemon is available with
  # a network connection instead of the default /var/run/docker.sock socket.
  #
  # The 'docker' hostname is the alias of the service container as described at
  # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
  #
  # Note that if you're using the Kubernetes executor, the variable should be set to
  # tcp://localhost:2375/ because of how the Kubernetes executor connects services
  # to the job container
  #DOCKER_HOST: tcp://localhost:2375/
  #
  # For non-Kubernetes executors, we use tcp://docker:2375/
  #  DOCKER_HOST: tcp://docker:2375/
  # When using dind, it's wise to use the overlayfs driver for
  # improved performance.
  DOCKER_DRIVER: overlay2
  #
  # This will instruct Docker not to start over TLS.
  DOCKER_TLS_CERTDIR: ""
  MAVEN_CLI_OPTS: "-s .m2/ci_settings.xml --batch-mode"
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

services:
  - docker:19.03.10-dind

stages:
  - test
  - build
  - release
  - package
  - benchmark

cache:
  paths:
    - .m2/repository/

.maven-release:
  stage: release
  variables:
    MAVEN_RELEASE_PLUGIN: "org.apache.maven.plugins:maven-release-plugin:2.5.3"
  script:
    - apk update
    - apk add git
    - git config user.name "${GITLAB_USER_NAME}"
    - git config user.email "${GITLAB_USER_EMAIL}"
    - git checkout ${CI_COMMIT_BRANCH}
    - ./mvnw ${MAVEN_OPTS} ${MAVEN_CLI_OPTS} clean ${MAVEN_RELEASE_PLUGIN}:prepare ${MAVEN_RELEASE_PLUGIN}:perform -DautoVersionSubmodules -Darguments="-DskipTests -DskipITs -Dmaven.javadoc.skip=true" -Dusername="gitlab-ci-token" -Dpassword=${CI_TAG_UPLOAD_TOKEN}
#  when: manual
#  only:
#    - master

.microbenchmarking:
  stage: benchmark
  script: |
    set -e
    java -jar microbenchmarking/target/benchmarks.jar

.docker-build:
  image: docker:19.03.1
  stage: package
  script:
    - docker info
    - echo $CI_REGISTRY
    - apk update
    - apk add openjdk8
    - export JAVA_HOME=/usr
    - export PROJECT_VERSION=$(./mvnw ${MAVEN_OPTS} ${MAVEN_CLI_OPTS} -q $MAVEN_OPTS $MAVEN_CLI_OPTS -Dexec.executable=echo -Dexec.args='${project.version}' --non-recursive exec:exec)
    - export DOCKER_TAG=$PROJECT_VERSION-$DOCKER_TAG_POSTFIX
    - echo $DOCKER_TAG
    - docker login -u mno1 -p $DOCKER_HUB_ACCESS_TOKEN
    - docker build --build-arg JAR_PATH -t mno1/prometheus-lab:$DOCKER_TAG .
    - docker push mno1/prometheus-lab:$DOCKER_TAG

.dependency-check-maven:
  stage: test
  variables:
    DEPENDENCY_CHECK_MAVEN_PLUGIN: "org.owasp:dependency-check-maven:6.0.2"
  script: |
    set -e
    ./mvnw ${MAVEN_CLI_OPTS} clean install -DskipTests
    ./mvnw ${MAVEN_CLI_OPTS} ${DEPENDENCY_CHECK_MAVEN_PLUGIN}:aggregate \
      -DfailBuildOnAnyVulnerability=true \
      -DgolangDepEnabled=false \
      -DassemblyAnalyzerEnabled=false \
      -DretireJsAnalyzerEnabled=false \
      -DnodeAnalyzerEnabled=false
  artifacts:
    when: always
    paths:
      - target/dependency-check-report.html
    expire_in: 7 days

code_quality:
  artifacts:
    paths: [gl-code-quality-report.json]
  tags:
    - docker

build-maven:
  stage: build
  script:
    - ./mvnw ${MAVEN_OPTS} ${MAVEN_CLI_OPTS} clean package -DskipTests
  artifacts:
    paths:
      - prometheus-lab-spring-web/target
      - prometheus-lab-spring-webflux/target
      - microbenchmarking/target/benchmarks.jar

test-maven:
  stage: test
  script:
    - ./mvnw ${MAVEN_OPTS} ${MAVEN_CLI_OPTS} clean test
  artifacts:
    reports:
      junit:
        - prometheus-lab-commons/target/surefire-reports/TEST-*.xml
        - prometheus-lab-spring-web/target/surefire-reports/TEST-*.xml
        - prometheus-lab-spring-webflux/target/surefire-reports/TEST-*.xml

dependency-check-maven:
  extends: .dependency-check-maven
  allow_failure: true

maven-release-merge:
  extends: .maven-release
  only:
    refs:
      - master
    variables:
      - $CI_COMMIT_MESSAGE =~ /^Merge branch/

microbenchmarking:
  extends: .microbenchmarking
  when: manual

docker-web-push-master:
  extends: .docker-build
  when: manual
  variables:
    JAR_PATH: prometheus-lab-spring-web
    DOCKER_TAG_POSTFIX: spring-web
  only:
    - tags

docker-webflux-push-master:
  extends: .docker-build
  when: manual
  variables:
    JAR_PATH: prometheus-lab-spring-webflux
    DOCKER_TAG_POSTFIX: spring-webflux
  only:
    - tags
