package codes.marcinnowak.prometheus.lab.test;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.web.util.UriComponentsBuilder;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(stubs = "classpath:/stubs/mappings", port = 0)
public abstract class PrometheusControllerNormalizedIncreaseAbstractIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void shouldQueryRangeIncrease() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "lab_nincrease(some_counter_metric[5s])")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "1.0")
                                .value(1600000008, "1.0")
                                .value(1600000009, "1.0")
                                .value(1600000010, "1.0")
                                .value(1600000011, "1.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "1.0")
                                .value(1600000018, "1.0")
                                .value(1600000019, "1.0")
                                .value(1600000020, "1.0")
                                .value(1600000021, "1.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "1.0")
                                .value(1600000028, "1.0")
                                .value(1600000029, "1.0")
                                .value(1600000030, "1.0")
                                .value(1600000031, "1.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldQueryRangeIncreaseWithMultiplication() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "lab_nincrease(some_counter_metric[2*5s])")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1600000001, "0.454546")  // interval
                                .value(1600000002, "0.454546")
                                .value(1600000003, "0.454546")
                                .value(1600000004, "0.454546")
                                .value(1600000005, "0.454546")
                                .value(1600000006, "0.454546")
                                .value(1600000007, "0.454546")
                                .value(1600000008, "0.454546")
                                .value(1600000009, "0.454546")
                                .value(1600000010, "0.454546")
                                .value(1600000011, "0.454546")  // interval
                                .value(1600000012, "0.5")
                                .value(1600000013, "0.5")
                                .value(1600000014, "0.5")
                                .value(1600000015, "0.5")
                                .value(1600000016, "0.5")
                                .value(1600000017, "0.5")
                                .value(1600000018, "0.5")
                                .value(1600000019, "0.5")
                                .value(1600000020, "0.5")
                                .value(1600000021, "0.5")  // interval
                                .value(1600000022, "0.5")
                                .value(1600000023, "0.5")
                                .value(1600000024, "0.5")
                                .value(1600000025, "0.5")
                                .value(1600000026, "0.5")
                                .value(1600000027, "0.5")
                                .value(1600000028, "0.5")
                                .value(1600000029, "0.5")
                                .value(1600000030, "0.5")
                                .value(1600000031, "0.5")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandel2Labels() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "lab_nincrease(some_counter_with_label[5s])")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_with_label")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "1.0")
                                .value(1600000008, "1.0")
                                .value(1600000009, "1.0")
                                .value(1600000010, "1.0")
                                .value(1600000011, "1.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "1.0")
                                .value(1600000018, "1.0")
                                .value(1600000019, "1.0")
                                .value(1600000020, "1.0")
                                .value(1600000021, "1.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "1.0")
                                .value(1600000028, "1.0")
                                .value(1600000029, "1.0")
                                .value(1600000030, "1.0")
                                .value(1600000031, "1.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_with_label")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val2")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "2.0")
                                .value(1600000008, "2.0")
                                .value(1600000009, "2.0")
                                .value(1600000010, "2.0")
                                .value(1600000011, "2.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "1.0")
                                .value(1600000018, "1.0")
                                .value(1600000019, "1.0")
                                .value(1600000020, "1.0")
                                .value(1600000021, "1.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "1.2")
                                .value(1600000028, "1.2")
                                .value(1600000029, "1.2")
                                .value(1600000030, "1.2")
                                .value(1600000031, "1.2")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelSum() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "sum(lab_nincrease(some_counter_with_label[5s]))")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_with_label")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "3.0")
                                .value(1600000008, "3.0")
                                .value(1600000009, "3.0")
                                .value(1600000010, "3.0")
                                .value(1600000011, "3.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "2.0")
                                .value(1600000018, "2.0")
                                .value(1600000019, "2.0")
                                .value(1600000020, "2.0")
                                .value(1600000021, "2.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "2.2")
                                .value(1600000028, "2.2")
                                .value(1600000029, "2.2")
                                .value(1600000030, "2.2")
                                .value(1600000031, "2.2")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelSumBy() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "sum(lab_nincrease(counter_with_label_ip_status[5s])) by (ip)")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("ip", "192.16.0.1")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "3.0")
                                .value(1600000008, "3.0")
                                .value(1600000009, "3.0")
                                .value(1600000010, "3.0")
                                .value(1600000011, "3.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "2.0")
                                .value(1600000018, "2.0")
                                .value(1600000019, "2.0")
                                .value(1600000020, "2.0")
                                .value(1600000021, "2.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "2.2")
                                .value(1600000028, "2.2")
                                .value(1600000029, "2.2")
                                .value(1600000030, "2.2")
                                .value(1600000031, "2.2")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("ip", "192.16.0.2")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0") // interval
                                .value(1600000007, "6.0")
                                .value(1600000008, "6.0")
                                .value(1600000009, "6.0")
                                .value(1600000010, "6.0")
                                .value(1600000011, "6.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0") // interval
                                .value(1600000017, "3.0")
                                .value(1600000018, "3.0")
                                .value(1600000019, "3.0")
                                .value(1600000020, "3.0")
                                .value(1600000021, "3.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "3.6")
                                .value(1600000028, "3.6")
                                .value(1600000029, "3.6")
                                .value(1600000030, "3.6")
                                .value(1600000031, "3.6")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelFilter() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "lab_nincrease(counter_with_label_ip_status{ip=\"localhost\"}[5s])")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "counter_with_label_ip_status")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("ip", "localhost")
                                .metric("status", "200 (OK)")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "1.0")
                                .value(1600000008, "1.0")
                                .value(1600000009, "1.0")
                                .value(1600000010, "1.0")
                                .value(1600000011, "1.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "1.0")
                                .value(1600000018, "1.0")
                                .value(1600000019, "1.0")
                                .value(1600000020, "1.0")
                                .value(1600000021, "1.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "1.0")
                                .value(1600000028, "1.0")
                                .value(1600000029, "1.0")
                                .value(1600000030, "1.0")
                                .value(1600000031, "1.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "counter_with_label_ip_status")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("ip", "localhost")
                                .metric("status", "400 (Bad Request)")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "2.0")
                                .value(1600000008, "2.0")
                                .value(1600000009, "2.0")
                                .value(1600000010, "2.0")
                                .value(1600000011, "2.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "1.0")
                                .value(1600000018, "1.0")
                                .value(1600000019, "1.0")
                                .value(1600000020, "1.0")
                                .value(1600000021, "1.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "1.2")
                                .value(1600000028, "1.2")
                                .value(1600000029, "1.2")
                                .value(1600000030, "1.2")
                                .value(1600000031, "1.2")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandleSumFilterInterruptedFirstMetric() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "sum(lab_nincrease(counter_with_label_ip_status_interrupted_first_begin{ip=\"localhost\"}[5s]))")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "counter_with_label_ip_status_interrupted_first_begin")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("ip", "localhost")
                                .metric("status", "200 (OK)")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "0.0")
                                .value(1600000008, "5.0")
                                .value(1600000009, "5.0")
                                .value(1600000010, "5.0")
                                .value(1600000011, "5.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "0.0")
                                .value(1600000018, "0.0")
                                .value(1600000019, "0.0")
                                .value(1600000020, "0.0")
                                .value(1600000021, "0.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "0.0")
                                .value(1600000028, "0.0")
                                .value(1600000029, "0.0")
                                .value(1600000030, "0.0")
                                .value(1600000031, "0.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelFilterInterruptedSecondMetric() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "sum(lab_nincrease(counter_with_label_ip_status_interrupted_second_begin{ip=\"localhost\"}[5s]))")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "counter_with_label_ip_status_interrupted_second_begin")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("ip", "localhost")
                                .metric("status", "200 (OK)")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "0.0")
                                .value(1600000008, "5.5")
                                .value(1600000009, "5.5")
                                .value(1600000010, "5.5")
                                .value(1600000011, "5.5")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "0.0")
                                .value(1600000018, "0.0")
                                .value(1600000019, "0.0")
                                .value(1600000020, "0.0")
                                .value(1600000021, "0.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "0.0")
                                .value(1600000028, "0.0")
                                .value(1600000029, "0.0")
                                .value(1600000030, "0.0")
                                .value(1600000031, "0.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelCounterWithCalculationFluctuationDuringTimeShift() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "sum(lab_nincrease(some_counter_metric[60s])) by (country)")
                        .queryParam("start", "1595880300")
                        .queryParam("end", "1595880660")
                        .queryParam("step", "15")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("country", "Germany")
                                .value(1595880300, "0.0")      // interval
                                .value(1595880315, "0.0")      // interval
                                .value(1595880330, "0.0")
                                .value(1595880345, "0.0")
                                .value(1595880360, "0.0")
                                .value(1595880375, "0.0")      // interval
                                .value(1595880390, "0.0")
                                .value(1595880405, "0.0")
                                .value(1595880420, "0.0")
                                .value(1595880435, "0.0")      // interval
                                .value(1595880450, "0.0")
                                .value(1595880465, "0.0")
                                .value(1595880480, "0.0")
                                .value(1595880495, "1.25")      // interval
                                .value(1595880510, "1.25")
                                .value(1595880525, "1.25")
                                .value(1595880540, "1.25")
                                .value(1595880555, "0.0")      // interval
                                .value(1595880570, "0.0")
                                .value(1595880585, "0.0")
                                .value(1595880600, "0.0")
                                .value(1595880615, "0.0")      // interval
                                .value(1595880630, "0.0")
                                .value(1595880645, "0.0")
                                .value(1595880660, "0.0")
                                .build())
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("country", "Poland")
                                .value(1595880300, "0.0")      // interval
                                .value(1595880315, "0.0")      // interval
                                .value(1595880330, "0.0")
                                .value(1595880345, "0.0")
                                .value(1595880360, "0.0")
                                .value(1595880375, "0.0")      // interval
                                .value(1595880390, "0.0")
                                .value(1595880405, "0.0")
                                .value(1595880420, "0.0")
                                .value(1595880435, "0.0")      // interval
                                .value(1595880450, "0.0")
                                .value(1595880465, "0.0")
                                .value(1595880480, "0.0")
                                .value(1595880495, "1.25")      // interval
                                .value(1595880510, "1.25")
                                .value(1595880525, "1.25")
                                .value(1595880540, "1.25")
                                .value(1595880555, "0.0")      // interval
                                .value(1595880570, "0.0")
                                .value(1595880585, "0.0")
                                .value(1595880600, "0.0")
                                .value(1595880615, "0.0")      // interval
                                .value(1595880630, "0.0")
                                .value(1595880645, "0.0")
                                .value(1595880660, "0.0")
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandleCounterWithCalculationFluctuationDuringTimeShiftShifted() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "sum(lab_nincrease(some_counter_metric[60s])) by (country)")
                        .queryParam("start", "1595880315")
                        .queryParam("end", "1595880675")
                        .queryParam("step", "15")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("country", "Germany")
                                .value(1595880315, "0.0")      // interval
                                .value(1595880330, "0.0")
                                .value(1595880345, "0.0")
                                .value(1595880360, "0.0")
                                .value(1595880375, "0.0")      // interval
                                .value(1595880390, "0.0")
                                .value(1595880405, "0.0")
                                .value(1595880420, "0.0")
                                .value(1595880435, "0.0")      // interval
                                .value(1595880450, "1.25")
                                .value(1595880465, "1.25")
                                .value(1595880480, "1.25")
                                .value(1595880495, "1.25")      // interval
                                .value(1595880510, "0.0")
                                .value(1595880525, "0.0")
                                .value(1595880540, "0.0")
                                .value(1595880555, "0.0")      // interval
                                .value(1595880570, "0.0")
                                .value(1595880585, "0.0")
                                .value(1595880600, "0.0")
                                .value(1595880615, "0.0")      // interval
                                .value(1595880630, "0.0")
                                .value(1595880645, "0.0")
                                .value(1595880660, "0.0")
                                .value(1595880675, "0.0")      // interval
                                .build())
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("country", "Poland")
                                .value(1595880315, "0.0")      // interval
                                .value(1595880330, "0.0")
                                .value(1595880345, "0.0")
                                .value(1595880360, "0.0")
                                .value(1595880375, "0.0")      // interval
                                .value(1595880390, "0.0")
                                .value(1595880405, "0.0")
                                .value(1595880420, "0.0")
                                .value(1595880435, "0.0")      // interval
                                .value(1595880450, "0.0")
                                .value(1595880465, "0.0")
                                .value(1595880480, "0.0")
                                .value(1595880495, "0.0")      // interval
                                .value(1595880510, "1.25")
                                .value(1595880525, "1.25")
                                .value(1595880540, "1.25")
                                .value(1595880555, "1.25")      // interval
                                .value(1595880570, "0.0")
                                .value(1595880585, "0.0")
                                .value(1595880600, "0.0")
                                .value(1595880615, "0.0")      // interval
                                .value(1595880630, "0.0")
                                .value(1595880645, "0.0")
                                .value(1595880660, "0.0")
                                .value(1595880675, "0.0")      // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelCounterWithCalculationFluctuationDuringTimeShiftShifted2() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "lab_nincrease(request_duration_seconds_count[10 * 10s])")
                        .queryParam("start", "1596099190")
                        .queryParam("end", "1596100540")
                        .queryParam("step", "10")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "request_duration_seconds_count")
                                .metric("instance", "some_instance")
                                .metric("label", "val1")
                                .metric("job", "some_job")
                                .value(1596099660, "0.75")
                                .value(1596099670, "0.75")
                                .value(1596099680, "0.75")
                                .value(1596099690, "0.75")
                                .value(1596099700, "1.2")
                                .value(1596099710, "1.2")
                                .value(1596099720, "1.2")
                                .value(1596099730, "1.2")
                                .value(1596099740, "1.2")
                                .value(1596099750, "1.2")
                                .value(1596099760, "1.2")
                                .value(1596099770, "1.2")
                                .value(1596099780, "1.2")
                                .value(1596099790, "1.2")
                                .value(1596099800, "0.7")
                                .value(1596099810, "0.7")
                                .value(1596099820, "0.7")
                                .value(1596099830, "0.7")
                                .value(1596099840, "0.7")
                                .value(1596099850, "0.7")
                                .value(1596099860, "0.7")
                                .value(1596099870, "0.7")
                                .value(1596099880, "0.7")
                                .value(1596099890, "0.7")
                                .value(1596099900, "0.0")
                                .value(1596099910, "0.0")
                                .value(1596099920, "0.0")
                                .value(1596099930, "0.0")
                                .value(1596099940, "0.0")
                                .value(1596099950, "0.0")
                                .value(1596099960, "0.0")
                                .value(1596099970, "0.0")
                                .value(1596099980, "0.0")
                                .value(1596099990, "0.0")
                                .value(1596100000, "0.0")
                                .value(1596100010, "0.0")
                                .value(1596100020, "0.0")
                                .value(1596100030, "0.0")
                                .value(1596100040, "0.0")
                                .value(1596100050, "0.0")
                                .value(1596100060, "0.0")
                                .value(1596100070, "0.0")
                                .value(1596100080, "0.0")
                                .value(1596100090, "0.0")
                                .value(1596100100, "0.0")
                                .value(1596100110, "0.0")
                                .value(1596100120, "0.0")
                                .value(1596100130, "0.0")
                                .value(1596100140, "0.0")
                                .value(1596100150, "0.0")
                                .value(1596100160, "0.0")
                                .value(1596100170, "0.0")
                                .value(1596100180, "0.0")
                                .value(1596100190, "0.0")
                                .value(1596100200, "0.0")
                                .value(1596100210, "0.0")
                                .value(1596100220, "0.0")
                                .value(1596100230, "0.0")
                                .value(1596100240, "0.0")
                                .value(1596100250, "0.0")
                                .value(1596100260, "0.0")
                                .value(1596100270, "0.0")
                                .value(1596100280, "0.0")
                                .value(1596100290, "0.0")
                                .value(1596100300, "0.0")
                                .value(1596100310, "0.0")
                                .value(1596100320, "0.0")
                                .value(1596100330, "0.0")
                                .value(1596100340, "0.0")
                                .value(1596100350, "0.0")
                                .value(1596100360, "0.0")
                                .value(1596100370, "0.0")
                                .value(1596100380, "0.0")
                                .value(1596100390, "0.0")
                                .value(1596100400, "0.0")
                                .value(1596100410, "0.0")
                                .value(1596100420, "0.0")
                                .value(1596100430, "0.0")
                                .value(1596100440, "0.0")
                                .value(1596100450, "0.0")
                                .value(1596100460, "0.0")
                                .value(1596100470, "0.0")
                                .value(1596100480, "0.0")
                                .value(1596100490, "0.0")
                                .value(1596100500, "0.0")
                                .value(1596100510, "0.0")
                                .value(1596100520, "0.0")
                                .value(1596100530, "0.0")
                                .value(1596100540, "0.0")
                                .build())
                        .build())
                .build());
    }

}
