package codes.marcinnowak.prometheus.lab.test;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.web.util.UriComponentsBuilder;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(stubs = "classpath:/stubs/mappings", port = 0)
public abstract class PrometheusControllerPassThroughAbstractTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void shouldQueryRangeCounter() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "some_counter_metric")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1600000001, "15")
                                .value(1600000002, "15")
                                .value(1600000003, "15")
                                .value(1600000004, "15")
                                .value(1600000005, "15")
                                .value(1600000006, "15")
                                .value(1600000007, "15")
                                .value(1600000008, "15")
                                .value(1600000009, "15")
                                .value(1600000010, "20")
                                .value(1600000011, "20")
                                .value(1600000012, "20")
                                .value(1600000013, "20")
                                .value(1600000014, "20")
                                .value(1600000015, "20")
                                .value(1600000016, "20")
                                .value(1600000017, "20")
                                .value(1600000018, "20")
                                .value(1600000019, "20")
                                .value(1600000020, "25")
                                .value(1600000021, "25")
                                .value(1600000022, "25")
                                .value(1600000023, "25")
                                .value(1600000024, "25")
                                .value(1600000025, "25")
                                .value(1600000026, "25")
                                .value(1600000027, "25")
                                .value(1600000028, "25")
                                .value(1600000029, "25")
                                .value(1600000030, "30")
                                .value(1600000031, "30")
                                .value(1600000032, "30")
                                .value(1600000033, "30")
                                .value(1600000034, "30")
                                .value(1600000035, "30")
                                .value(1600000036, "30")
                                .value(1600000037, "30")
                                .value(1600000038, "30")
                                .value(1600000039, "30")
                                .value(1600000040, "30")
                                .build())
                        .build())
                .build());
    }

}
